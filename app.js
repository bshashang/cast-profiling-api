const express = require('express');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const mongoose = require('mongoose');
const cors = require('cors');
const authRouter = require('./routes/auth');

const app = express();

// const { getUser } = require('./middleware');

const MONGOURL = 'mongodb://localhost:27017/cast_profiling';
mongoose.set('useNewUrlParser', true);
mongoose.set('useFindAndModify', false);
mongoose.set('useCreateIndex', true);
mongoose.set('useUnifiedTopology', true);
mongoose.connect(MONGOURL).then(() => console.log('mongodb connected to ', MONGOURL)).catch((err) => console.log(err));

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use('/auth', authRouter);

module.exports = app;
