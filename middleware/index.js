
let jwt = require('jsonwebtoken');
const redis = require('redis');
const config = require('../config.js');
var user = require('../db-services/user');

const client = redis.createClient();

client.on('error', (err) => {
  console.log("Error " + err);
});

const validate = async (req) => {
  let token = req.headers['x-access-token'] || req.headers['authorization'];
  if (token === undefined) {
    return false;
  }
  if (token.startsWith('Bearer ')) {
    token = token.slice(7, token.length);
  }
  if (token) {
    return jwt.verify(token, config.secret, (err, decoded) => {
      if (err) {
        return false
      } else {
        return decoded;
      }
    });
  } else {
    return false;
  }
};

exports.getUser = async (req, res, next) => {
  const validation = await validate(req);
  if(validation) {
    return client.get(String(validation.userId), async function (err, result) {
      if (err) console.log(err);
      if (result) {
        req.user = JSON.parse(result);
        return next();
      }
      else {
        const userData = await user.getUserById(validation.userId);
        if (userData._id) {
          client.setex(String(userData._id), 86400, JSON.stringify(userData), (err) => console.log(err));
          req.user = userData;
          return next();
        }
      }
      res.send({success: false, errorMessage: "Invalid Token"})
    })
  }
  res.send({success: false, errorMessage: "Invalid Token"})
}