const UserModel = require('../models/user');

exports.getUser = (username) => UserModel.findOne({ username },
  (err) => { if (err) console.log(err); });

exports.setUser = (data) => {
  const entry = new UserModel(data);
  return entry.save();
};

exports.getUserById = (_id) => UserModel.findOne({ _id }, (err) => { if (err) console.log(err); });
