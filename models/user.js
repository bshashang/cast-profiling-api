const mongoose = require('mongoose');

const { Schema } = mongoose;

const userSchema = new Schema({
  email: {
    type: String,
    unique: true,
    trim: true,
    requried: true,
  },
  password: { type: String, trim: true, requried: true },
});
const UserModel = mongoose.model('UserModel', userSchema, 'user');

module.exports = UserModel;
