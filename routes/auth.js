const express = require('express');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const config = require('../config');
const user = require('../db-services/user');

const router = express.Router();

/* Login */
router.post('/login', async (req, res) => {
  try {
    const userData = await user.getUser(req.body.email);
    if (userData) {
      bcrypt.compare(req.body.password, userData.password, (err, result) => {
        if (err) res.send({ status: false, error: 'Invalid email or password' });
        if (result) {
          const token = jwt.sign({ userId: userData._id, email: userData.email }, config.secret, { expiresIn: '1h' });
          res.send({ success: true, token, data: userData });
        } else {
          res.send({ status: false, error: 'Invalid email or password' });
        }
      });
    } else {
      res.send({ status: false, error: 'Invalid email or password' });
    }
  } catch (err) {
    res.send({ success: false, error: 'something went wrong' });
  }
});

/* Signup */
router.post('/signup', async (req, res) => {
  try {
    const hsh = await bcrypt.hash(req.body.password, 10);
    await user.setUser({ email: req.body.email, password: hsh });
    res.send({ success: true });
  } catch (err) {
    res.send({ success: false, error: 'something went wrong' });
  }
});
module.exports = router;
